#ifndef ITERATOR_H
#define ITERATOR_H

class Iterator {
public:
	Iterator();
	virtual ~Iterator() = default;

	virtual Iterator *first() const = 0;
	virtual Iterator *next() = 0;
	virtual int &get() const = 0;
	virtual bool isDone() const = 0;
};

#endif // ITERATOR_H
