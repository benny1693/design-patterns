#ifndef ARRAYLIST_H
#define ARRAYLIST_H

#include "iterator.h"
#include "list.h"

class ArrayList : public List {
private:
	int size;
	int *array;

public:
	ArrayList(int);

	int getSize() const;
	int &get(int) const;
	Iterator *createIterator() const;
};

#endif // ARRAYLIST_H
