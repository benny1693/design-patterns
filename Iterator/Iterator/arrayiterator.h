#ifndef ARRAYITERATOR_H
#define ARRAYITERATOR_H

#include "arraylist.h"
#include "iterator.h"

class ArrayIterator : public Iterator {
private:
	const ArrayList *array;
	int position;

public:
	ArrayIterator(const ArrayList *array, int = 0);

	virtual Iterator *first() const;
	virtual Iterator *next();
	virtual int &get() const;
	virtual bool isDone() const;
};

#endif // ARRAYITERATOR_H
