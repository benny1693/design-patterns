#ifndef LIST_H
#define LIST_H

#include "iterator.h"

class List {
public:
	List();
	virtual ~List() = default;

	virtual int getSize() const = 0;
	virtual int &get(int) const = 0;
	virtual Iterator *createIterator() const = 0;
};

#endif // LIST_H
