#include "arrayiterator.h"

ArrayIterator::ArrayIterator(const ArrayList *a, int pos)
		: array(a), position(pos) {}

Iterator *ArrayIterator::first() const { return new ArrayIterator(array, 0); }

Iterator *ArrayIterator::next() {
	if (!isDone())
		position += 1;
	return this;
}

int &ArrayIterator::get() const { return array->get(position); }

bool ArrayIterator::isDone() const { return position == array->getSize(); }
