#include "arraylist.h"
#include "arrayiterator.h"

ArrayList::ArrayList(int s) : size(s ? s : 10), array(new int[size]) {
	for (int i = 0; i < size; ++i)
		array[i] = 0;
}

int ArrayList::getSize() const { return size; }

int &ArrayList::get(int i) const { return array[i]; }

Iterator *ArrayList::createIterator() const { return new ArrayIterator(this); }
