#include "arraylist.h"
#include <iostream>

int main() {
	ArrayList array(10);

	for (int i = 1; i < array.getSize(); i += 2)
		array.get(i) = 1;

	for (Iterator *it = array.createIterator(); !it->isDone(); it->next())
		std::cout << it->get() << " ";

	std::cout << std::endl;

	return 0;
}
