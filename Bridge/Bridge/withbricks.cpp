#include "withbricks.h"

WithBricks::WithBricks() {}

std::string WithBricks::material() const {
	string topbottom = "++++++++";
	string sides = "|       ";

	string upborder = topbottom + topbottom + topbottom + topbottom;
	string LRborder = sides + sides + sides;

	string lines = upborder + "+\n" + LRborder + sides + "|\n" + upborder + "+\n|   " + LRborder  + "|   |\n";

	return lines + lines + upborder + "+";
}
