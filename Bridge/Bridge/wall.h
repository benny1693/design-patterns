#ifndef WALL_H
#define WALL_H

#include "howtobuildwall.h"
#include "iostream"

class Wall {
private:
	HowToBuildWall* howTo;
public:
	Wall(HowToBuildWall*);
	virtual ~Wall() = default;

	virtual void build() const;

};

#endif // WALL_H
