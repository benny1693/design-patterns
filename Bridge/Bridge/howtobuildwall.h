#ifndef HOWTOBUILDWALL_H
#define HOWTOBUILDWALL_H

#include<string>
using std::string;

class HowToBuildWall
{
public:
	HowToBuildWall();
	virtual ~HowToBuildWall() = default;

	virtual string material() const = 0;
};

#endif // HOWTOBUILDWALL_H
