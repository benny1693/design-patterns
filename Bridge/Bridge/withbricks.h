#ifndef WITHBRICKS_H
#define WITHBRICKS_H

#include "howtobuildwall.h"

class WithBricks : public HowToBuildWall
{
public:
	WithBricks();

	string material() const;
};

#endif // WITHBRICKS_H
