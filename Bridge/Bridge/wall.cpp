#include "wall.h"

Wall::Wall(HowToBuildWall *ht) : howTo(ht) {}

void Wall::build() const { std::cout << howTo->material() << std::endl; }
