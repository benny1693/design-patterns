#ifndef WITHCEMENT_H
#define WITHCEMENT_H

#include "howtobuildwall.h"

class WithCement : public HowToBuildWall {
public:
	WithCement();

	string material() const;
};

#endif // WITHCEMENT_H
