#ifndef WALLBUILDED_H
#define WALLBUILDED_H

#include "wall.h"

class WallBuilded : public Wall {
public:
	WallBuilded(HowToBuildWall*);
};

#endif // WALLBUILDED_H
