#include "wallbuilded.h"
#include "withcement.h"
#include "withbricks.h"

int main()
{

	WallBuilded bricks(new WithBricks);
	WallBuilded cement(new WithCement);

	bricks.build();
	cement.build();

	return 0;
}
