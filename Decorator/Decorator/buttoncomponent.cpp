#include "buttoncomponent.h"

ButtonComponent::ButtonComponent(QString label, QWidget *parent)
		: VisualComponent(parent) {
	setButton(label);
}

void ButtonComponent::setButton(QString label) {
	button = new QPushButton(label, this);
	addWidget(button);
}

void ButtonComponent::show() { QWidget::show(); }
