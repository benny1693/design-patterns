#include "visualcomponent.h"

VisualComponent::VisualComponent(QWidget *parent) : QWidget(parent), layout(new QVBoxLayout) {
	setLayout(layout);
}

void VisualComponent::addWidget(QWidget* widget)
{
	layout->addWidget(widget);
}
