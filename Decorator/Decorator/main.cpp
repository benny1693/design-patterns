#include <QApplication>
#include "buttoncomponent.h"
#include "lister.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	Lister app(new ButtonComponent("ciaone"));

	app.show();

	return a.exec();
}
