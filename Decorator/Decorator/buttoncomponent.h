#ifndef BUTTONCOMPONENT_H
#define BUTTONCOMPONENT_H

#include "visualcomponent.h"



class ButtonComponent : public VisualComponent {
private:
	QPushButton *button;

public:
	ButtonComponent(QString, QWidget* =0);

	virtual void setButton(QString);

	void show();
};

#endif // BUTTONCOMPONENT_H
