#ifndef VISUALCOMPONENT_H
#define VISUALCOMPONENT_H

#include <QtWidgets>

class VisualComponent : public QWidget {
	Q_OBJECT
private:
	QVBoxLayout* layout;

public:
	VisualComponent(QWidget *parent = 0);
	~VisualComponent() = default;

	virtual void addWidget(QWidget*);

	virtual void show() = 0;
};

#endif // VISUALCOMPONENT_H
