#include "lister.h"

Lister::Lister(VisualComponent *vc) : Decorator(vc) {
	addList();
}

void Lister::addList()
{
	addWidget(new QListWidget(getToDecorate()));
}
