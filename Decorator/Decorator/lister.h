#ifndef LISTER_H
#define LISTER_H

#include "decorator.h"

class Lister : public Decorator {
public:
	Lister(VisualComponent *);

	void addList();
};

#endif // LISTER_H
