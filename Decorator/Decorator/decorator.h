#ifndef DECORATOR_H
#define DECORATOR_H

#include "visualcomponent.h"

class Decorator : public VisualComponent {
private:
	VisualComponent* toDecorate;
public:
	Decorator(VisualComponent*);

	VisualComponent* getToDecorate() const;

	void show();
};

#endif // DECORATOR_H
