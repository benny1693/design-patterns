#include "decorator.h"

Decorator::Decorator(VisualComponent *td)
	: VisualComponent(td), toDecorate(td) {}

VisualComponent* Decorator::getToDecorate() const
{
	return toDecorate;
}

void Decorator::show() { toDecorate->show(); }
