#include "multiplication.h"

Multiplication::Multiplication(int a, int b) : x(a), y(b) {}

int Multiplication::compute() const
{
	return x*y;
}
