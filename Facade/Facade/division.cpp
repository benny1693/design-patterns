#include "division.h"

Division::Division(int a, int b) : x(a), y(b) {}

int Division::compute() const
{
	return x/y;
}
