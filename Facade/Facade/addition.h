#ifndef ADD_H
#define ADD_H

#include "operator.h"

class Addition : public Operator
{
private:
	int x,y;
public:
	Addition(int,int);

	int compute() const;
};

#endif // ADD_H
