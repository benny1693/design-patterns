#ifndef DIVISION_H
#define DIVISION_H

#include "operator.h"

class Division : public Operator
{
private:
	int x,y;
public:
	Division(int,int);

	int compute() const;
};

#endif // DIVISION_H
