#ifndef CALCULATOR_H
#define CALCULATOR_H

#include "addition.h"
#include "difference.h"
#include "division.h"
#include "multiplication.h"

class Calculator {
private:
	int result;
public:
	Calculator();
	int add(int, int) ;
	int diff(int, int) ;
	int times(int, int) ;
	int divide(int, int) ;
	void printResult() const;
};

#endif // CALCULATOR_H
