#ifndef MULTIPLICATION_H
#define MULTIPLICATION_H

#include "operator.h"

class Multiplication : public Operator
{
private:
	int x,y;
public:
	Multiplication(int,int);

	int compute() const;
};

#endif // MULTIPLICATION_H
