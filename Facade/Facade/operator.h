#ifndef OPERATOR_H
#define OPERATOR_H


class Operator
{
public:
	Operator();
	virtual ~Operator() = default;

	virtual operator int();

	virtual int compute() const = 0;
};

#endif // OPERATOR_H
