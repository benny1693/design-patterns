#ifndef DIFFERENCE_H
#define DIFFERENCE_H

#include "operator.h"

class Difference : public Operator
{
private:
	int x,y;
public:
	Difference(int,int);
	int compute() const;
};

#endif // DIFFERENCE_H
