#include "calculator.h"
#include <iostream>

Calculator::Calculator() : result(0) {}

int Calculator::add(int a, int b) { return result = *(new Addition(a, b)); }

int Calculator::diff(int a, int b) { return result = *(new Difference(a, b)); }

int Calculator::times(int a, int b) {
	return result = *(new Multiplication(a, b));
}

int Calculator::divide(int a, int b) { return result = *(new Division(a, b)); }

void Calculator::printResult() const { std::cout << result << std::endl; }
