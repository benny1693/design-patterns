#include "addition.h"

Addition::Addition(int a, int b) : x(a), y(b) {}

int Addition::compute() const {
	return x+y;
}
