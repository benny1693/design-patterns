#include "difference.h"

Difference::Difference(int a, int b) : x(a), y(b) {}

int Difference::compute() const {
	return x-y;
}
