#ifndef ACTIONPCGAME_H
#define ACTIONPCGAME_H

#include "actiongame.h"

class ActionPCGame : public ActionGame
{
public:
	ActionPCGame(Genre);

	int getFramePerSec() const;
};

#endif // ACTIONPCGAME_H
