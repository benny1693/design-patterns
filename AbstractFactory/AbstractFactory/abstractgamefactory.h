#ifndef ABSTRACTGAMEFACTORY_H
#define ABSTRACTGAMEFACTORY_H

#include "actiongame.h"
#include "rpggame.h"

class AbstractGameFactory {
public:
	virtual ~AbstractGameFactory() = default;

	virtual ActionGame* createActionGame() const = 0;
	virtual RPGGame* createRPGGame() const = 0;
};

#endif // ABSTRACTGAMEFACTORY_H
