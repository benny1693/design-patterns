#include "ps4gamefactory.h"
#include "rpgps4game.h"
#include "actionps4game.h"
#include "genre.h"

PS4GameFactory::PS4GameFactory() {}
ActionGame *PS4GameFactory::createActionGame() const { return new ActionPS4Game(Genre::FPS); }
RPGGame * PS4GameFactory::createRPGGame() const { return new RPGPS4Game(Genre::Strategic); }
