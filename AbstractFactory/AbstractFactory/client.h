#ifndef CLIENT_H
#define CLIENT_H

#include "abstractgamefactory.h"
#include "actiongame.h"
#include "rpggame.h"

#include <vector>
using std::vector;

class Client {
private:
	AbstractGameFactory *factory;

	vector<ActionGame *> actionGames;
	vector<RPGGame *> rpgGames;

public:
	Client(AbstractGameFactory *);

	void setFactory(AbstractGameFactory *);
	AbstractGameFactory* getFactory() const;

	void addActionGame();
	void addRPGGame();

	void printGames() const;
};

#endif // CLIENT_H
