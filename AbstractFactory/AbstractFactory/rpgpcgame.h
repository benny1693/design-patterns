#ifndef RPGPCGAME_H
#define RPGPCGAME_H

#include "rpggame.h"

class RPGPCGame : public RPGGame {
public:
	RPGPCGame(Genre);

	int getFramePerSec() const;
};
#endif // RPGPCGAME_H
