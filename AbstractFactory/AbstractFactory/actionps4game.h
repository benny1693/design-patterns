#ifndef ACTIONPS4GAME_H
#define ACTIONPS4GAME_H

#include "actiongame.h"

class ActionPS4Game : public ActionGame {
public:
	ActionPS4Game(Genre);

	int getFramePerSec() const;
};

#endif // ACTIONPS4GAME_H
