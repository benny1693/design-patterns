#include <iostream>

#include "client.h"
#include "pcgamefactory.h"
#include "ps4gamefactory.h"

void test(Client& client) {
	for (int i = 0; i < 5; ++i) {
		client.addActionGame();
	}

	for (int i = 0; i < 4; ++i) {
		client.addRPGGame();
	}

	client.printGames();
}

int main()
{

	Client client(new PCGameFactory());

	test(client);

	client.setFactory(new PS4GameFactory());

	test(client);

	std::cout << "end" << std::endl;

	return 0;
}
