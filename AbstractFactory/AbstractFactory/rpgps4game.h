#ifndef RPGPS4GAME_H
#define RPGPS4GAME_H

#include "rpggame.h"

class RPGPS4Game : public RPGGame {
public:
	RPGPS4Game(Genre);

	int getFramePerSec() const;
};

#endif // RPGPS4GAME_H
