#include "pcgamefactory.h"
#include "rpgpcgame.h"
#include "actionpcgame.h"
#include "genre.h"

PCGameFactory::PCGameFactory() {}
ActionGame *PCGameFactory::createActionGame() const { return new ActionPCGame(Genre::FPS); }
RPGGame * PCGameFactory::createRPGGame() const { return new RPGPCGame(Genre::Strategic); }
