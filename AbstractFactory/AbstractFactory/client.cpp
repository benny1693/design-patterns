#include "client.h"
#include <iostream>

Client::Client(AbstractGameFactory *f) : factory(f) {}

void Client::setFactory(AbstractGameFactory *f) { factory = f; }

AbstractGameFactory *Client::getFactory() const { return factory; }

void Client::addActionGame() { actionGames.push_back(factory->createActionGame()); }

void Client::addRPGGame() { rpgGames.push_back(factory->createRPGGame()); }

void Client::printGames() const {
	for (ActionGame *game : actionGames) {
		std::cout << game->getFramePerSec() << " ";
	}

	std::cout << std::endl;

	for (RPGGame *game : rpgGames) {
		std::cout << game->getFramePerSec() << " ";
	}

	std::cout << std::endl;
}
