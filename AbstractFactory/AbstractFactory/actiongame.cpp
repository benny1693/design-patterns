#include "actiongame.h"

ActionGame::ActionGame(Genre g) : genre(g) {}

Genre ActionGame::getGenre() const { return genre; }
