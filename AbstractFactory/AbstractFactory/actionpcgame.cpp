#include "actionpcgame.h"

ActionPCGame::ActionPCGame(Genre g) : ActionGame(g) {}

int ActionPCGame::getFramePerSec() const { return 60; }
