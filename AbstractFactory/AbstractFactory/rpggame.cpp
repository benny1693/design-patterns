#include "rpggame.h"

RPGGame::RPGGame(Genre g) : genre(g) {}

Genre RPGGame::getGenre() const { return genre; }
