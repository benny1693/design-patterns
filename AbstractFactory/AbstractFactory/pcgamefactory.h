#ifndef PCGAMEFACTORY_H
#define PCGAMEFACTORY_H

#include "abstractgamefactory.h"

class PCGameFactory : public AbstractGameFactory {
public:
	PCGameFactory();
	virtual ~PCGameFactory() = default;

	virtual ActionGame* createActionGame() const;
	virtual RPGGame* createRPGGame() const;
};

#endif // PCGAMEFACTORY_H
