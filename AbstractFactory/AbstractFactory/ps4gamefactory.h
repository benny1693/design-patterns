#ifndef PS4GAMEFACTORY_H
#define PS4GAMEFACTORY_H

#include "abstractgamefactory.h"

class PS4GameFactory : public AbstractGameFactory
{
public:
	PS4GameFactory();
	virtual ~PS4GameFactory() = default;

	virtual ActionGame* createActionGame() const;
	virtual RPGGame* createRPGGame() const;
};

#endif // PS4GAMEFACTORY_H
