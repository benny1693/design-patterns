#ifndef ACTIONGAME_H
#define ACTIONGAME_H

#include "genre.h"

class ActionGame {
private:
	Genre genre;

public:
	ActionGame(Genre);
	virtual ~ActionGame() = default;

	Genre getGenre() const;

	virtual int getFramePerSec() const = 0;
};

#endif // ACTIONGAME_H
