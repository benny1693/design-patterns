#ifndef RPGGAME_H
#define RPGGAME_H

#include "genre.h"

class RPGGame {
private:
	Genre genre;

public:
	RPGGame(Genre);
	virtual ~RPGGame() = default;

	Genre getGenre() const;

	virtual int getFramePerSec() const = 0;
};

#endif // RPGGAME_H
