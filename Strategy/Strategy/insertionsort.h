#ifndef INSERTIONSORT_H
#define INSERTIONSORT_H

#include "sortingalgorithm.h"

class InsertionSort : public SortingAlgorithm
{
public:
	InsertionSort();

	int* sort(int const[10]) const;
};

#endif // INSERTIONSORT_H
