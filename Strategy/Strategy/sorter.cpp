#include "sorter.h"

Sorter::Sorter() {
	for (int i = 0; i < 10; ++i)
		array[i] = 10 - i;
}

void Sorter::setAlgorithm(SortingAlgorithm *alg) { algorithm = alg; }

int *Sorter::sort() const { return algorithm->sort(array); }
