#include "mergesort.h"

void MergeSort::merge(int *array, int start, int half, int end) const {
	const int leftSize = half - start +1;
	const int rightSize = end - half;

	int left[leftSize + 1], right[rightSize + 1];

	for (int i = 0; i < leftSize; ++i)
		left[i] = array[start + i];
	for (int i = 0; i < rightSize; ++i)
		right[i] = array[half + i +1];

	left[leftSize] = 1000;
	right[rightSize] = 1000;

	int i = 0;
	int j = 0;

	for (int k = start; k <= end; ++k) {
		if (left[i] <= right[j]) {
			array[k] = left[i];
			++i;
		} else {
			array[k] = right[j];
			++j;
		}
	}
}

void MergeSort::mergesort(int *array, int start, int end) const {
	if (start < end) {
		int half = (end + start) / 2;
		mergesort(array, start, half);
		mergesort(array, half + 1, end);
		merge(array, start, half, end);
	}
}

MergeSort::MergeSort() {}

int *MergeSort::sort(const int array[10]) const {
	int *res = new int[10];

	for (int i = 0; i < 10; ++i)
		res[i] = array[i];

	mergesort(res,0,9);

	return res;
}
