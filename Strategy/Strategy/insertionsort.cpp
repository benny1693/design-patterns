#include "insertionsort.h"

InsertionSort::InsertionSort() {}

int *InsertionSort::sort(const int array[10]) const {

	int *res = new int[10];

	for (int i = 0; i < 10; ++i)
		res[i] = array[i];

	for (int i = 1; i < 10; ++i) {
		int key = res[i];

		int j = 0;
		for (j = i - 1; j >= 0 && res[j] > key; --j)
			res[j + 1] = res[j];

		res[j + 1] = key;
	}

	return res;
}
