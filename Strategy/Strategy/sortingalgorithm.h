#ifndef SORTINGALGORITHM_H
#define SORTINGALGORITHM_H

class SortingAlgorithm {
public:
	SortingAlgorithm();
	virtual ~SortingAlgorithm() = default;

	virtual int *sort(int const[10]) const =0;
};

#endif // SORTINGALGORITHM_H
