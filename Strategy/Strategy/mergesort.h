#ifndef MERGESORT_H
#define MERGESORT_H

#include "sortingalgorithm.h"

class MergeSort : public SortingAlgorithm
{
private:
	void merge(int*,int,int,int) const;
	void mergesort(int*,int,int) const;
public:
	MergeSort();
	int* sort(int const[10]) const;
};

#endif // MERGESORT_H
