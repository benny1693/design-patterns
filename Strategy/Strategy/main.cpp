#include "insertionsort.h"
#include "mergesort.h"
#include "sorter.h"
#include<iostream>

void print(int*);

int main()
{
	MergeSort* merge = new MergeSort;
	InsertionSort* insertion = new InsertionSort;
	Sorter sorter;

	int* array;

	sorter.setAlgorithm(insertion);
	std::cout << "InsertionSort" << std::endl;
	array = sorter.sort();
	print(array);
	delete[] array;

	sorter.setAlgorithm(merge);
	std::cout << "MergeSort" << std::endl;
	array = sorter.sort();
	print(array);
	delete[] array;

	return 0;
}

void print(int* array){
	for (int i = 0 ; i<10; ++i)
		std::cout<<array[i]<<" ";
	std::cout<<std::endl;
}
