#ifndef SORTER_H
#define SORTER_H

#include "sortingalgorithm.h"

class Sorter {
private:
	SortingAlgorithm* algorithm;
	int array[10];
public:
	Sorter();
	void setAlgorithm(SortingAlgorithm*);

	int* sort() const;
};

#endif // SORTER_H
