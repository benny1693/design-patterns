#ifndef NODE_H
#define NODE_H

#include<vector>
#include<string>
using std::vector;
using std::string;

class Node {
public:
	Node();
	virtual ~Node() = default;

	virtual void addSuccessor(Node*) =0;
	virtual vector<Node*> getSuccessors() const =0;
	virtual void printSuccessors() const =0;
};

#endif // NODE_H
