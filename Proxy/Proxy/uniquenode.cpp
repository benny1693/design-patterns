#include "uniquenode.h"
#include <iostream>

UniqueNode::UniqueNode(string l) : label(l), references(0) {}

UniqueNode::UniqueNode(const UniqueNode &n)
		: label(n.label), successors(n.successors), references(0) {}

std::string UniqueNode::getLabel() const { return label; }

void UniqueNode::addSuccessor(Node *newNode) { successors.push_back(newNode); }

vector<Node *> UniqueNode::getSuccessors() const { return successors; }

void UniqueNode::printSuccessors() const {
	for (Node *n : successors)
		std::cout << static_cast<UniqueNode *>(n)->label << " ";
}

unsigned int UniqueNode::decreaseReference() { return --references; }

unsigned int UniqueNode::incrementReference() { return ++references; }

unsigned int UniqueNode::getReference() { return references; }
