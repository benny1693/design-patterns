#include "proxynode.h"
#include "uniquenode.h"

#include <iostream>

int main() {
	UniqueNode *A = new UniqueNode("A");
	UniqueNode *B = new UniqueNode("B");
	UniqueNode *C = new UniqueNode("C");

	ProxyNode *P1 = new ProxyNode(A);
	ProxyNode *P2 = new ProxyNode(A);
	ProxyNode *P3 = new ProxyNode(B);
	ProxyNode *P4 = new ProxyNode(C);

	std::cout << A->getReference() << " " << B->getReference() << " "
						<< C->getReference() << std::endl;

	P1->addSuccessor(P3);
	P1->addSuccessor(P4);
	P1->addSuccessor(P1);
	P2->addSuccessor(B);

	P1->printSuccessors();
	std::cout << std::endl;

	P2->printSuccessors();
	std::cout << std::endl;

	return 0;
}
