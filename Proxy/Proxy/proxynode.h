#ifndef PROXYNODE_H
#define PROXYNODE_H

#include "node.h"
#include "uniquenode.h"

class ProxyNode : public Node {
private:
	UniqueNode *node;
	UniqueNode* getNode() const;

public:
	ProxyNode(UniqueNode*);

	virtual void addSuccessor(Node *);
	virtual vector<Node *> getSuccessors() const;
	virtual void printSuccessors() const;

	virtual string operator*() const;
	virtual UniqueNode* operator->() const;
};

#endif // PROXYNODE_H
