#ifndef UNIQUENODE_H
#define UNIQUENODE_H

#include "node.h"

class UniqueNode : public Node {
private:
	string label;
	vector<Node*> successors;
	unsigned int references;

public:
	UniqueNode(string);
	UniqueNode(const UniqueNode&);
	string getLabel() const;

	virtual void addSuccessor(Node*);
	virtual vector<Node*> getSuccessors() const;
	virtual void printSuccessors() const;

	unsigned int decreaseReference();
	unsigned int incrementReference();
	unsigned int getReference();
};

#endif // UNIQUENODE_H
