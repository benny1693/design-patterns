#include "proxynode.h"

UniqueNode *ProxyNode::getNode() const { return node; }

ProxyNode::ProxyNode(UniqueNode *n) : node(n) { node->incrementReference(); }

void ProxyNode::addSuccessor(Node *newNode) {
	UniqueNode *uNode = dynamic_cast<UniqueNode *>(newNode);
	if (!uNode)
		uNode = static_cast<ProxyNode *>(newNode)->getNode();

	node->decreaseReference();
	UniqueNode *copy = new UniqueNode(*node);

	if (node->getReference() <= 0)
		delete node;

	node = copy;
	node->incrementReference();

	node->addSuccessor(uNode);
}

vector<Node *> ProxyNode::getSuccessors() const {
	vector<Node *> result;
	for (Node *n : node->getSuccessors()) {
		result.push_back(new ProxyNode(static_cast<UniqueNode *>(n)));
	}

	return result;
}

void ProxyNode::printSuccessors() const {
	node->printSuccessors();
}

std::string ProxyNode::operator*() const { return node->getLabel(); }

UniqueNode *ProxyNode::operator->() const { return node; }
