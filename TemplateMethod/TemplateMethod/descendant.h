#ifndef DESCENDANT_H
#define DESCENDANT_H

#include "printer.h"

class Descendant : public Printer {
public:
	Descendant(std::string);

	std::string order(std::string) const;
};

#endif // DESCENDANT_H
