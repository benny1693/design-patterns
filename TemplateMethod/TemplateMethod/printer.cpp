#include "printer.h"
#include <iostream>

Printer::Printer(std::string s) : subject(s) {}

void Printer::print() const { std::cout << order(subject) << std::endl; }
