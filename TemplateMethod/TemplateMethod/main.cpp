#include "ascendant.h"
#include "descendant.h"

int main() {
	Printer *desc = new Descendant("Hello");
	Printer *asc = new Ascendant("Hello");

	desc->print();
	asc->print();

	return 0;
}
