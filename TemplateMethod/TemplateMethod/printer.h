#ifndef PRINTER_H
#define PRINTER_H

#include<string>

class Printer
{
private:
	std::string subject;
public:
	Printer(std::string);
	virtual ~Printer() = default;

	void print() const;
	virtual std::string order(std::string) const =0;
};

#endif // PRINTER_H
