#ifndef ASCENDANT_H
#define ASCENDANT_H

#include "printer.h"

class Ascendant : public Printer {
public:
	Ascendant(std::string);
	std::string order(std::string) const;
};

#endif // ASCENDANT_H
