#include "descendant.h"

Descendant::Descendant(std::string s) : Printer(s) {}

std::string Descendant::order(std::string str) const {
	std::string result = "";
	for (auto it = str.rbegin(); it != str.rend(); ++it)
		result.push_back(*it);

	return result;
}
