#include "printcommand.h"

PrintCommand::PrintCommand(std::string c) : command(c) {}

void PrintCommand::setCommand(std::string c) {
	command = c;
	notify();
}

std::string PrintCommand::getCommand() const {
	return command;
}
