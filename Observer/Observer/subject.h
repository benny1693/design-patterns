#ifndef SUBJECT_H
#define SUBJECT_H

#include<vector>
#include"observer.h"

class Subject
{
private:
std::vector<Observer*> observers;
protected:
	Subject();
public:
	virtual ~Subject() = default;

	void notify() const;
	void attach(Observer*);
	void detach(Observer*);
};

#endif // SUBJECT_H
