#ifndef PRINTCOMMAND_H
#define PRINTCOMMAND_H

#include "subject.h"
#include <string>

class PrintCommand : public Subject {
private:
	std::string command;
public:
	PrintCommand(std::string);
	void setCommand(std::string);
	std::string getCommand() const;
};

#endif // PRINTCOMMAND_H
