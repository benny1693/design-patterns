#include "printer.h"
#include <iostream>

Printer::Printer(PrintCommand *p, std::string name)
		: printerName(name), command(p) {}

void Printer::update() {
	std::cout << "From " + printerName + ": '" + command->getCommand() + "'"
						<< std::endl;
}
