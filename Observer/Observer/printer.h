#ifndef PRINTER_H
#define PRINTER_H

#include "observer.h"
#include "printcommand.h"
#include <string>

class Printer : public Observer {
private:
	std::string printerName;
	PrintCommand *command;

public:
	Printer(PrintCommand *, std::string printerName);

	void update();
};

#endif // PRINTER_H
