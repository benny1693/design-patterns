#include "subject.h"

Subject::Subject() {}

void Subject::notify() const {
	for (Observer* i : observers)
		i->update();
}

void Subject::attach(Observer * obs) {
	observers.push_back(obs);
}

void Subject::detach(Observer * obs) {
	for (auto it = observers.begin(); it != observers.end(); ++it){
		if (*it == obs){
			observers.erase(it);
			return;
		}
	}
}
