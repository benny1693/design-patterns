#include <iostream>

#include "printcommand.h"
#include "printer.h"

using std::cout;
using std::cin;

int main() {
	PrintCommand* command = new PrintCommand("hello");
	Printer* printer1 = new Printer(command,"printer1");
	Printer* printer2 = new Printer(command,"printer2");
	command->attach(printer1);
	command->attach(printer2);

	std::string str;
	cin >> str;
	while (str != "quit") {
		command->setCommand(str);
		cin >> str;
	}

	command->detach(printer2);

	cin >> str;
	while (str != "quit") {
		command->setCommand(str);
		cin >> str;
	}

	std::cout << std::endl;

	return 0;
}
