#ifndef A4PRINTER_H
#define A4PRINTER_H

#include "printerhandler.h"
#include "photoprinter.h"

class A4Printer : public PrinterHandler {
public:
	A4Printer();

	void print(string) const;
};

#endif // A4PRINTER_H
