#include "a4printer.h"

A4Printer::A4Printer() : PrinterHandler(new PhotoPrinter(), "A4") {}

void A4Printer::print(std::string request) const {
	if (this->code == request)
		cout << "A4 sheet printed\n";
	else
		PrinterHandler::print(request);
}
