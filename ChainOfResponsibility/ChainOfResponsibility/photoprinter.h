#ifndef PHOTOPRINTER_H
#define PHOTOPRINTER_H

#include "printerhandler.h"
#include "ticketprinter.h"

class PhotoPrinter : public PrinterHandler{
public:
	PhotoPrinter();

	void print(string) const;
};

#endif // PHOTOPRINTER_H
