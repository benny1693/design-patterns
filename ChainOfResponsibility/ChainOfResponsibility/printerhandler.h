#ifndef PRINTERHANDLER_H
#define PRINTERHANDLER_H

#include <string>
#include<iostream>
using std::string;
using std::cout;

class PrinterHandler {
private:
	PrinterHandler *successor;

protected:
	string code;
	PrinterHandler(PrinterHandler *,string);

public:

	virtual ~PrinterHandler() = default;

	virtual void print(string) const;
};

#endif // PRINTERHANDLER_H
