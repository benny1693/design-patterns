#include "printerhandler.h"

PrinterHandler::PrinterHandler(PrinterHandler *ph, string code)
		: successor(ph), code(code) {}

void PrinterHandler::print(std::string request) const {
	if (successor)
		successor->print(request);
	else
		cout << "Non disponibile\n";
}
