#include "ticketprinter.h"

TicketPrinter::TicketPrinter()
		: PrinterHandler(0, "TK") {}

void TicketPrinter::print(std::string request) const {
	if (this->code == request)
		cout << "Ticket printed\n";
	else
		PrinterHandler::print(request);
}
