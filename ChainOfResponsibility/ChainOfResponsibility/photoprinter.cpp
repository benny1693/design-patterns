#include "photoprinter.h"

PhotoPrinter::PhotoPrinter()
		: PrinterHandler(new TicketPrinter(), "PH") {}

void PhotoPrinter::print(std::string request) const {
	if (this->code == request)
		cout << "Photo printed\n";
	else
		PrinterHandler::print(request);
}
