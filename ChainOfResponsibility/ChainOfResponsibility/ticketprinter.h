#ifndef TICKETPRINTER_H
#define TICKETPRINTER_H

#include "printerhandler.h"

class TicketPrinter : public PrinterHandler {
public:
	TicketPrinter();

	void print(string) const;
};

#endif // TICKETPRINTER_H
