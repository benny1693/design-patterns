#ifndef LINUXCREATOR_H
#define LINUXCREATOR_H

#include "abstractcreator.h"
#include "linuxfilesystem.h"

class LinuxCreator : public AbstractCreator {
public:
	LinuxCreator();
	~LinuxCreator() = default;
	LinuxFileSystem* CreateFileSystem() const;
};


#endif // LINUXCREATOR_H
