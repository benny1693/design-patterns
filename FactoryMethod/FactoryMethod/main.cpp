#include "filesystem.h"
#include "linuxcreator.h"
#include "wincreator.h"
#include "parametrizedcreator.h"

int main()
{
	AbstractCreator* windowsCreator = new WinCreator;
	AbstractCreator* linuxCreator = new LinuxCreator;

	FileSystem* Windows = windowsCreator->CreateFileSystem();
	FileSystem* Linux = linuxCreator->CreateFileSystem();

	Windows->readAndPrint("ciao");
	Linux->readAndPrint("ciaone");

	ParametrizedCreator creator;

	FileSystem* Windows2 = creator.CreateFileSystem(ParametrizedCreator::WINDOWS);
	FileSystem* Linux2 = creator.CreateFileSystem(ParametrizedCreator::LINUX);

	Windows2->readAndPrint("ciao1");
	Linux2->readAndPrint("ciaone1");

	return 0;
}
