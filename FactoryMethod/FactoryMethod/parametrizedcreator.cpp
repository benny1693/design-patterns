#include "parametrizedcreator.h"

ParametrizedCreator::ParametrizedCreator() {}

FileSystem *ParametrizedCreator::CreateFileSystem(ParametrizedCreator::Family f) {
	if (f == Family::LINUX)
		return new LinuxFileSystem;
	else
		return new WinFileSystem;
}
