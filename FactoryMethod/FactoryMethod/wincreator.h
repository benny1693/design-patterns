#ifndef WINCREATOR_H
#define WINCREATOR_H

#include "abstractcreator.h"
#include "winfilesystem.h"

class WinCreator : public AbstractCreator {
public:
	WinCreator();
	~WinCreator() = default;
	WinFileSystem* CreateFileSystem() const;
};

#endif // WINCREATOR_H
