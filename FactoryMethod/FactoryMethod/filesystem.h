#ifndef FILESYSTEM_H
#define FILESYSTEM_H

#include<string>
using std::string;

class FileSystem {
private:
	string fileContent;
public:
	FileSystem(string = "");
	virtual ~FileSystem() = default;

	virtual void printFileContent() const = 0;

	string getFileContent() const;

	void readAndPrint(string);
};

#endif // FILESYSTEM_H
