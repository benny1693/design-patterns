#ifndef LINUXFILESYSTEM_H
#define LINUXFILESYSTEM_H

#include "filesystem.h"

class LinuxFileSystem : public FileSystem {
public:
	LinuxFileSystem(string = "");
	~LinuxFileSystem() = default;

	void printFileContent() const;
};

#endif // LINUXFILESYSTEM_H
