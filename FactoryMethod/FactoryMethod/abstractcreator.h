#ifndef ABSTRACTCREATOR_H
#define ABSTRACTCREATOR_H

#include "filesystem.h"

class AbstractCreator {
public:
	AbstractCreator();
	virtual ~AbstractCreator() = default;

	virtual FileSystem* CreateFileSystem() const = 0;
};

#endif // ABSTRACTCREATOR_H
