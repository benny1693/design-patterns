#include "winfilesystem.h"
#include <iostream>

WinFileSystem::WinFileSystem(string s) : FileSystem(s) {}

void WinFileSystem::printFileContent() const {
	std::cout << "Windows: " << getFileContent() << std::endl;
}
