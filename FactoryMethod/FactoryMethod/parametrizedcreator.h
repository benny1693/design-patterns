#ifndef PARAMETRIZEDCREATOR_H
#define PARAMETRIZEDCREATOR_H

#include "linuxfilesystem.h"
#include "winfilesystem.h"

class ParametrizedCreator {
public:

	enum Family {LINUX, WINDOWS};

	ParametrizedCreator();

	FileSystem* CreateFileSystem(Family);
};

#endif // PARAMETRIZEDCREATOR_H
