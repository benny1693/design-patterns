#include "filesystem.h"

FileSystem::FileSystem(string s) : fileContent(s) {}

std::string FileSystem::getFileContent() const { return fileContent; }

void FileSystem::readAndPrint(string content) {
	fileContent = content;
	printFileContent();
}
