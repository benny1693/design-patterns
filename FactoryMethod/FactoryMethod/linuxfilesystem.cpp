#include "linuxfilesystem.h"
#include<iostream>

LinuxFileSystem::LinuxFileSystem(string s) : FileSystem(s) {}

void LinuxFileSystem::printFileContent() const {
	std::cout << "Linux: " << getFileContent() << std::endl;
}
