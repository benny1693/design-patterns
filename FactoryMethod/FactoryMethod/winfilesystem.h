#ifndef WINFILESYSTEM_H
#define WINFILESYSTEM_H

#include "filesystem.h"

class WinFileSystem : public FileSystem {
public:
	WinFileSystem(string = "");
	~WinFileSystem() = default;

	void printFileContent() const;
};

#endif // WINFILESYSTEM_H
