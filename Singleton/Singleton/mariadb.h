#ifndef MARIADB_H
#define MARIADB_H

#include "database.h"

class MariaDB : public Database
{
public:
	MariaDB();

	void printDatabaseType() const;
};

#endif // MARIADB_H
