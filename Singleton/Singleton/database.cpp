#include "database.h"
#include <exception>

map<string,Database*> Database::registry = map<string,Database*>();
string Database::DB = "initDB()";

Database::Database() {}

Database *Database::lookUp(string name) {
	if (registry.find(name) != registry.end()) {
		return registry.at(name);
	} else
		return nullptr;
}

void Database::registerInstance(string name, Database *newInstance) {
	registry.insert(std::pair<string, Database *>(name, newInstance));
}

Database *Database::getInstance(std::string name) {
	Database *dbInstance = lookUp(name);
	if (!dbInstance)
		throw std::exception();

	return dbInstance;
}

void Database::printDatabaseType() const {
	std::cout << "I can be any kind of DB!" << std::endl;
}

void Database::printAllInstances() {
	for (auto it = registry.begin(); it != registry.end(); ++it)
		it->second->printDatabaseType();
}
