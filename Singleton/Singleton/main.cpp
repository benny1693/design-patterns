#include "firebase.h"
#include "mariadb.h"

int main()
{
	Firebase* firebase = new Firebase;
	Firebase* firebase2 = new Firebase;
	MariaDB* mariadb = new MariaDB;

	try {
		Database* f = Database::getInstance("Firebase");
		std::cout << f << "  " << firebase << "\n";
		f = Database::getInstance("Firebase");
		std::cout << f << "  " << firebase << "\n";
	} catch (std::exception) {
		std::cout << "Can't get an instance of Firebase" << std::endl;
	}

	Database::printAllInstances();

	return 0;
}
