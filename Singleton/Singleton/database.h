#ifndef DATABASE_H
#define DATABASE_H

#include <map>
#include <string>
#include<iostream>

using std::string;
using std::map;

class Database {
private:
	static map<string, Database *> registry; //useful for subclassing in Singleton
	static string DB;

protected:
	Database();
	virtual ~Database() = default;
	static Database *lookUp(string); //useful for subclassing in Singleton

public:
	static void registerInstance(string, Database *); //useful for subclassing in Singleton
	static Database* getInstance(string);

	virtual void printDatabaseType() const;

	static void printAllInstances();
};

#endif // DATABASE_H
