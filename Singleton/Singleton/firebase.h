#ifndef FIREBASE_H
#define FIREBASE_H

#include "database.h"

class Firebase : public Database {
public:
	Firebase();

	void printDatabaseType() const;
};

#endif // FIREBASE_H
