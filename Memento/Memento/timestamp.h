#ifndef TIMESTAMP_H
#define TIMESTAMP_H

#include "mementotimestamp.h"

class Timestamp {
private:
	long seconds;
public:
	Timestamp();

	MementoTimestamp* createMemento() const;
	void setMemento(MementoTimestamp*);

	void update();

	void printTime() const;
};

#endif // TIMESTAMP_H
