#include "mementotimestamp.h"

void MementoTimestamp::setSeconds(long m) { seconds = m; }

long MementoTimestamp::getSeconds() const { return seconds; }

MementoTimestamp::MementoTimestamp(long m) : seconds(m) {}
