#include "timestamp.h"
#include <chrono>
#include <iostream>

using namespace std::chrono;

Timestamp::Timestamp()
		: seconds(system_clock::to_time_t((system_clock::now()))) {}

MementoTimestamp *Timestamp::createMemento() const {
	return new MementoTimestamp(seconds);
}

void Timestamp::setMemento(MementoTimestamp *memento) {
	seconds = memento->getSeconds();
}

void Timestamp::update() {
	seconds = system_clock::to_time_t((system_clock::now()));
}

void Timestamp::printTime() const {
	std::cout << seconds << std::endl;
}
