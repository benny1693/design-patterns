#include "timestamp.h"
#include<iostream>

// Il main funge da caretaker
int main()
{
	Timestamp time;
	MementoTimestamp* memento = time.createMemento();

	std::cout << "Valore iniziale: ";
	time.printTime();

	char c = '\0';
	while (c != 'q'){
		time.update();
		time.printTime();
		std::cin >> c;
	}
	time.update();
	std::cout << "Valore finale: ";
	time.printTime();
	time.setMemento(memento);
	std::cout << "Memento: ";
	time.printTime();

	return 0;
}
