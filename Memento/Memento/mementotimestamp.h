#ifndef MEMENTOTIMESTAMP_H
#define MEMENTOTIMESTAMP_H

class Timestamp;

class MementoTimestamp
{
	friend class Timestamp;
private:
	long seconds;
	void setSeconds(long);
	long getSeconds() const;
public:
	MementoTimestamp(long);

};

#endif // MEMENTOTIMESTAMP_H
