#ifndef CARDCOLLECTION_H
#define CARDCOLLECTION_H

#include<vector>
#include "collectionelement.h"
#include "card.h"
#include "deck.h"

using std::vector;

class CardCollection {
private:
	vector<CollectionElement*> collection;

	CollectionElement* findElement(string name) const;

public:
	CardCollection();

	CollectionElement* getElement(string name);

	Deck* createDeck() const;

	void addCardToDeck(CollectionElement*,Deck*) const;

	void printVarieties() const;

};

#endif // CARDCOLLECTION_H
