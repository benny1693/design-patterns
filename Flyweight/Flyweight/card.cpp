#include "card.h"
#include <iostream>

Card::Card(string n) : name(n) {}

std::string Card::getName() const { return name; }

void Card::printElement() const { std::cout << name << " "; }
