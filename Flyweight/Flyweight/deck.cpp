#include "deck.h"
#include<iostream>

Deck::Deck() {}

std::string Deck::getName() const { return ""; }

void Deck::addCard(CollectionElement* element)
{
	children.push_back(element);
}

void Deck::printElement() const {
	std::cout << "DECK [";
	for (CollectionElement *element : children)
		element->printElement();
	std::cout << "] ";
}
