#ifndef DECK_H
#define DECK_H

#include "collectionelement.h"
#include<vector>
using std::vector;

class Deck : public CollectionElement {
private:
	vector<CollectionElement*> children;
public:
	Deck();

	std::string getName() const;

	void addCard(CollectionElement*);

	void printElement() const;
};

#endif // DECK_H
