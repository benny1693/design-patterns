#include "cardcollection.h"
#include<vector>
#include<iostream>

int main() {
	CardCollection collection;

	std::vector<CollectionElement*> collectedElements;

	collectedElements.push_back(collection.getElement("mago"));
	collectedElements.push_back(collection.getElement("guerriero"));
	collectedElements.push_back(collection.getElement("ladro"));
	collectedElements.push_back(collection.getElement("mago"));
	collectedElements.push_back(collection.getElement("mago"));
	Deck* deck1 = collection.createDeck();
	collectedElements.push_back(deck1);
	collection.addCardToDeck(collection.getElement("mago"),deck1);
	collection.addCardToDeck(collection.getElement("guerriero"),deck1);

	for (CollectionElement* el : collectedElements)
		el->printElement();

	std::cout << std::endl;

	collection.printVarieties();

	std::cout << std::endl;

	return 0;
}
