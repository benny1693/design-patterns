#include "cardcollection.h"

CardCollection::CardCollection() {}

CollectionElement *CardCollection::findElement(std::string name) const {
	CollectionElement *result = 0;
	for (auto it = collection.begin(); it != collection.end() && !result; ++it) {
		if ((*it)->getName() == name)
			result = *it;
	}
	return result;
}

CollectionElement *CardCollection::getElement(string name) {
	CollectionElement *elementFound = findElement(name);
	if (elementFound)
		return elementFound;

	collection.push_back(new Card(name));
	return *(collection.rbegin());
}

Deck *CardCollection::createDeck() const { return new Deck; }

void CardCollection::addCardToDeck(CollectionElement* element,Deck* deck) const {
	deck->addCard(element);
}

void CardCollection::printVarieties() const
{
	for (CollectionElement* el : collection)
		el->printElement();
}
