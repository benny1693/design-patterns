#ifndef COLLECTIONELEMENT_H
#define COLLECTIONELEMENT_H

#include<string>

class CollectionElement {
public:
	CollectionElement();
	virtual ~CollectionElement() = default;

	virtual std::string getName() const =0;

	virtual void printElement() const =0;
};

#endif // COLLECTIONELEMENT_H
