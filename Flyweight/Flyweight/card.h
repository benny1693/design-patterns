#ifndef CARD_H
#define CARD_H

#include "collectionelement.h"
#include<string>
using std::string;

class Card : public CollectionElement {
private:
	string name;
public:
	Card(string);

	string getName() const;

	void printElement() const;
};

#endif // CARD_H
