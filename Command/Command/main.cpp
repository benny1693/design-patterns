#include "device.h"
#include "echo.h"
#include "user.h"

int main() {
	/* Il receiver (device : Device*) potrebbe trovarsi da tutt'altra parte
	 * (magari potrebbe essere un campo dati qualche altra classe) ed essere
	 * semplicemente passato come parametro, ovvero non è necessario conoscere
	 * l'esatta collocazione dell'oggetto*/

	Device *device = new Device();

	User user(new Echo(device, "Hello!"));

	return 0;
}
