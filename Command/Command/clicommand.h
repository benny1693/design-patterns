#ifndef CLICOMMAND_H
#define CLICOMMAND_H

class CLICommand {
public:
	CLICommand();
	virtual ~CLICommand() = default;

	virtual void run() const =0;
};

#endif // CLICOMMAND_H
