#ifndef DEVICE_H
#define DEVICE_H

#include<string>

class Device
{
public:
	Device();

	void print(std::string) const;
};

#endif // DEVICE_H
