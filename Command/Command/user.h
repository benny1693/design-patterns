#ifndef USER_H
#define USER_H

#include "clicommand.h"
#include<string>
using std::string;

class User
{
private:
	CLICommand* command;
public:
	User(CLICommand*);
};

#endif // USER_H
