#ifndef ECHO_H
#define ECHO_H

#include "clicommand.h"
#include "device.h"
#include<string>

class Echo : public CLICommand {
private:
	Device* device;
	std::string input;

public:
	Echo(Device*,std::string);

	void run() const;
};

#endif // ECHO_H
