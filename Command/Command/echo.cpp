#include "echo.h"
#include <iostream>

Echo::Echo(Device* d, std::string i) : device(d), input(i) {}

void Echo::run() const { device->print(input); }
