#include "simplevisitor.h"

SimpleVisitor::SimpleVisitor() {}
std::string SimpleVisitor::visitImage(const Image *image) const {
	return "\n+  " + image->getHex() + "  \n";
}

std::string SimpleVisitor::visitText(const Text *text) const {
	return "\n+  " + text->getDescription() + "  \n";
}
