#ifndef SIMPLEVISITOR_H
#define SIMPLEVISITOR_H

#include "visitor.h"
#include "image.h"
#include "text.h"

class SimpleVisitor : public Visitor
{
public:
	SimpleVisitor();
	virtual std::string visitImage(const Image *) const;
	virtual std::string visitText(const Text *) const;
};

#endif // SIMPLEVISITOR_H
