#ifndef VISITOR_H
#define VISITOR_H

#include <string>

class Image;
class Text;

class Visitor {
public:
	Visitor();
	virtual ~Visitor() = default;

	virtual std::string visitImage(const Image *) const = 0;
	virtual std::string visitText(const Text *) const = 0;
};

#endif // VISITOR_H
