#ifndef CHAPTER_H
#define CHAPTER_H

#include "chapterelement.h"
#include "prettyvisitor.h"
#include "simplevisitor.h"
#include <vector>

class Chapter {
private:
	std::vector<ChapterElement *> elements;

public:
	Chapter();
	void addElement(ChapterElement*);

	void print() const;
	void prettyPrint() const;
};

#endif // CHAPTER_H
