#include "text.h"

std::string Text::getDescription() const { return description; }

Text::Text(std::string d) : description(d) {}

std::string Text::accept(Visitor *v) const { return v->visitText(this); }
