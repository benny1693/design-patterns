#include "image.h"
#include<cstdlib>
#include<stdlib.h>

Image::Image(int h) : hex(h) {}

std::string Image::accept(Visitor* v) const
{
	return v->visitImage(this);
}

std::string Image::loadImage() const {
	return std::to_string(hex);
}

std::string Image::getHex() const {
	return loadImage();
}
