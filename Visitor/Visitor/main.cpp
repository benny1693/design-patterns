#include "chapter.h"

int main() {

	Chapter chapter;
	chapter.addElement(new Text("ciao"));
	chapter.addElement(new Image(1));

	chapter.print();
	chapter.prettyPrint();

	return 0;
}
