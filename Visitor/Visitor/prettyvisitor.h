#ifndef PRETTYVISITOR_H
#define PRETTYVISITOR_H

#include "visitor.h"
#include "image.h"
#include "text.h"

class PrettyVisitor : public Visitor
{
public:
	PrettyVisitor();
	virtual std::string visitImage(const Image *) const;
	virtual std::string visitText(const Text *) const;
};

#endif // PRETTYVISITOR_H
