#ifndef IMAGE_H
#define IMAGE_H

#include "chapterelement.h"

class Image : public ChapterElement
{
private:
	int hex;
public:
	Image(int);

	std::string accept(Visitor*) const;
	std::string loadImage() const;
	std::string getHex() const;
};

#endif // IMAGE_H
