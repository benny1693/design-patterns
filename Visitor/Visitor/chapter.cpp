#include "chapter.h"
#include <iostream>

Chapter::Chapter() {}

void Chapter::addElement(ChapterElement *ce) { elements.push_back(ce); }

void Chapter::print() const {
	std::string result = "";
	Visitor *v = new SimpleVisitor();
	for (ChapterElement *ce : elements)
		result += ce->accept(v);
	delete v;

	std::cout << result;
}

void Chapter::prettyPrint() const {
	std::string result = "";
	Visitor *v = new PrettyVisitor();
	for (ChapterElement *ce : elements)
		result += ce->accept(v);
	delete v;

	std::cout << result;
}
