#ifndef TEXT_H
#define TEXT_H

#include "chapterelement.h"

class Text : public ChapterElement
{
private:
	std::string description;
public:
	Text(std::string);

	virtual std::string accept(Visitor*) const;
	std::string getDescription() const;
};

#endif // TEXT_H
