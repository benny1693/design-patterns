#ifndef CHAPTERELEMENT_H
#define CHAPTERELEMENT_H

#include "visitor.h"
#include <string>

class ChapterElement {
public:
	ChapterElement();
	virtual ~ChapterElement() = default;

	virtual std::string accept(Visitor *) const =0;
};

#endif // CHAPTERELEMENT_H
