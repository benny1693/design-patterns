#include "prettyvisitor.h"

PrettyVisitor::PrettyVisitor() {}

std::string PrettyVisitor::visitImage(const Image *image) const {

	std::string imString = image->getHex();
	std::string result = "";
	for (unsigned int i = 0; i < imString.size() + 6; ++i)
		result += "+";

	result += "\n+  " + imString + "  +\n";

	for (unsigned int i = 0; i < imString.size() + 6; ++i)
		result += "+";

	return result += "\n";
}

std::string PrettyVisitor::visitText(const Text *text) const {
	std::string textString = text->getDescription();
	std::string result = "";
	for (unsigned int i = 0; i < textString.size() + 6; ++i)
		result += "+";

	result += "\n+  " + textString + "  +\n";

	for (unsigned int i = 0; i < textString.size() + 6; ++i)
		result += "+";

	return result += "\n";
}

