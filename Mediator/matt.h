#ifndef MATT_H
#define MATT_H

#include"anchorman.h"
#include"sarah.h"

class Matt : public AnchorMan
{
private:
Sarah* sarah;
public:
	Matt(Sarah*);

	void giveTheFloor() const;
};

#endif // MATT_H
