#include "matt.h"

Matt::Matt(Sarah *s) : AnchorMan("Matt"), sarah(s) {}

void Matt::giveTheFloor() const { sarah->answer(); }
