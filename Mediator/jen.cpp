#include "jen.h"

Jen::Jen(Jim *j) : AnchorMan("Jen"), jim(j) {}

void Jen::giveTheFloor() const { jim->answer(); }
