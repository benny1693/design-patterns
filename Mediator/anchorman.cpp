#include "anchorman.h"

AnchorMan::AnchorMan(std::string n) : name(n) {}

void AnchorMan::print(std::string phrase) const {
	std::cout << phrase << std::endl;
}
