#include"jim.h"
#include"jen.h"
#include"sarah.h"
#include"matt.h"

int main()
{
	Jim* jim = new Jim;
	Sarah* sarah = new Sarah;

	Matt* matt = new Matt(sarah);
	Jen* jen = new Jen(jim);

	jim->setAnchorman(jen);
	sarah->setAnchorman(matt);

	std::cout << "First report" << std::endl;
	matt->giveTheFloor();
	jen->giveTheFloor();
	std::cout << "-------------------------------------" << std::endl;

	jim->setAnchorman(matt);
	sarah->setAnchorman(jen);

	std::cout << "Second report" << std::endl;
	matt->giveTheFloor();
	jen->giveTheFloor();
	std::cout << "-------------------------------------" << std::endl;

	return 0;
}
