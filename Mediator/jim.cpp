#include "jim.h"

Jim::Jim(AnchorMan *a) : Journalist(a) {}

void Jim::answer() const {
	getAnchorMan()->print("Jim from New York to "+getAnchorMan()->name);
}
