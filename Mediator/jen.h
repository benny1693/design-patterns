#ifndef JEN_H
#define JEN_H

#include "anchorman.h"
#include "jim.h"

class Jen : public AnchorMan
{
private:
	Jim* jim;
public:
	Jen(Jim*);

	void giveTheFloor() const;
};

#endif // JEN_H
