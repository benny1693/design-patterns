#include "sarah.h"

Sarah::Sarah(AnchorMan *a) : Journalist(a) {}

void Sarah::answer() const {
	getAnchorMan()->print("Sarah from Seattle to "+getAnchorMan()->name);
}
