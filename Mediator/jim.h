#ifndef JIM_H
#define JIM_H

#include "journalist.h"

class Jim : public Journalist
{
public:
	Jim(AnchorMan* =0);

	void answer() const;
};

#endif // JIM_H
