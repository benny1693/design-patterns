#ifndef ANCHORMAN_H
#define ANCHORMAN_H

#include<iostream>
#include<string>

class AnchorMan {
public:
	std::string name;
	AnchorMan(std::string);
	virtual ~AnchorMan() = default;

	virtual void giveTheFloor() const = 0;
	virtual void print(std::string) const;
};

#endif // ANCHORMAN_H
