#include "journalist.h"

void Journalist::setAnchorman(AnchorMan *value) { anchorman = value; }

Journalist::Journalist(AnchorMan *a) : anchorman(a) {}

AnchorMan *Journalist::getAnchorMan() const { return anchorman; }
