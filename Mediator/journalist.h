#ifndef JOURNALIST_H
#define JOURNALIST_H

#include "anchorman.h"

class Journalist {
private:
	AnchorMan *anchorman;

public:
	Journalist(AnchorMan * = 0);
	virtual ~Journalist() = default;

	virtual void answer() const = 0;
	virtual AnchorMan *getAnchorMan() const;
	void setAnchorman(AnchorMan *value);
};

#endif // JOURNALIST_H
