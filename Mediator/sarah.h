#ifndef SARAH_H
#define SARAH_H

#include "journalist.h"

class Sarah : public Journalist
{
public:
	Sarah(AnchorMan* =0);

	void answer() const;
};

#endif // SARAH_H
