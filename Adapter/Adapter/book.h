#ifndef BOOK_H
#define BOOK_H

#include<list>
#include<string>
using std::list;
using std::string;
class Book {
private:
	class Page {
	public:
		string text;
		Page(string ="");
		string readPage() const;
	};

	list<Page> pages;
public:
	Book();
	virtual ~Book() = default;

	virtual void insertPage(string = "");
	virtual string readBook() const;
};

#endif // BOOK_H
