#include "pdf2book.h"
#include <iostream>
using std::cout;
using std::endl;

int main() {

	Book book;
	book.insertPage("OK: Hello there!");
	book.insertPage("GG: General Kenobi!");

	PDF pdf("OK: Hello there!\nGG: General Kenobi!");

	PDF2Book pdf2book(pdf);

	cout << book.readBook() << endl;
	cout << pdf.readPDF() << endl;
	cout << endl;
	cout << pdf2book.readBook() << endl;

	return 0;
}
