#include "pdf.h"

PDF::PDF(string t) : text(t) {}

void PDF::insertText(string t) {
	text += t;
}

std::string PDF::readPDF() const {
	return text;
}
