#ifndef PDF2BOOK_H
#define PDF2BOOK_H

#include "book.h"
#include "pdf.h"

class PDF2Book : public Book {
private:
	PDF pdf;

	list<string> pdfSubstrings() const;
	string totalBook(list<string>) const;
public:
	PDF2Book(PDF);

	void insertPage(string);
	string readBook() const;
};

#endif // PDF2BOOK_H
