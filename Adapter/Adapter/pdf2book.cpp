#include "pdf2book.h"
#include<iostream>

list<std::string> PDF2Book::pdfSubstrings() const {
	list<string> substrings;
	u_int start = 0;
	u_int end = 0;
	while (end < pdf.readPDF().size()) {
		if (pdf.readPDF()[end] == '\n'){
			substrings.push_back(pdf.readPDF().substr(start,end));
			start = end+1;
		}
		++end;
	}

	if (pdf.readPDF().substr(start,end).size())
		substrings.push_back(pdf.readPDF().substr(start,end));

	return substrings;
}

std::string PDF2Book::totalBook(list<std::string> substrings) const {
	string result;
	string endpage = "\n----------------------------------\n";
	for (string sub : substrings) {
		result += sub + endpage;
	}
	return result;
}

PDF2Book::PDF2Book(PDF p) : pdf(p) {}

void PDF2Book::insertPage(std::string text) { pdf.insertText(text); }

std::string PDF2Book::readBook() const {
	list<string> substrings = pdfSubstrings();
	return totalBook(substrings);
}
