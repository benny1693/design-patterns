#include "book.h"

Book::Book() {}

void Book::insertPage(string text) { pages.push_back(Page(text)); }

std::string Book::readBook() const {
	string result = "";
	for (Page page : pages)
		result += page.readPage() + "\n----------------------------------\n";
	return result;
}

Book::Page::Page(string t) : text(t) {}

std::string Book::Page::readPage() const {
	return text;
}
