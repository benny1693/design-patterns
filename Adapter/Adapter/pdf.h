#ifndef PDF_H
#define PDF_H

#include<string>
using std::string;

class PDF {
private:
string text;
public:
	PDF(string ="");

	void insertText(string);
	string readPDF() const;
};

#endif // PDF_H
