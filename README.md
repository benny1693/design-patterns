# Catalogo dei design patterns in C++
Questa repository contiene una implementazione in C++ di tutti i design pattern descritti in "Design Patterns: Elementi per il riuso di software a oggetti" della "Gang of Four" (Gamma, Helm, Johnson, Vlissides).

Per la codifica dei pattern ho utilizzato QtCreator e, in particolare, l'implementazione di Decorator fa uso della libreria Qt per la creazione di una semplicissima interfaccia grafica (priva di qualunque connessione a un modello dei dati).

## Build
La repository è dotata dei file <span style="font-family:monospace">.pro</span> necessari per effettuare l'operazione di build. In questo caso, una volta muniti della libreria Qt (che trovate in versione "Community" dal <a href="https://www.qt.io/download">sito ufficiale</a>, basterà lanciare i comandi:
- <span style="font-family:monospace">qmake file.pro</span>
- make

## Informazioni
- Versione di Qt: 5.10.0
- Versione di C++: C++11
- OS: Linux Ubuntu 17.10

