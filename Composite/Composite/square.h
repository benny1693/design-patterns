#ifndef SQUARE_H
#define SQUARE_H

#include "shape.h"

class Square : public Shape {
private:
	int side;
	void drawHorizontal() const;
public:
	Square(int,Shape* =nullptr);
	~Square() = default;

	void draw() const;
};

#endif // SQUARE_H
