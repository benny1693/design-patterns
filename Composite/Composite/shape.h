#ifndef SHAPE_H
#define SHAPE_H

class Shape {
private:
	Shape* parent;
public:
	Shape(Shape* =nullptr);
	virtual ~Shape() = default;

	virtual void addChild(Shape*);
	virtual void removeChild(Shape*);

	virtual void draw() const = 0;
};

#endif // SHAPE_H
