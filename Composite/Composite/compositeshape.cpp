#include "compositeshape.h"

CompositeShape::CompositeShape(Shape *s) : Shape(s) {}

CompositeShape::~CompositeShape() {
	for (Shape *child : children)
		delete child;
}

void CompositeShape::addChild(Shape *s) { children.push_back(s); }

void CompositeShape::removeChild(Shape *s) {
	bool deleted = false;
	for (auto it = children.begin(); it != children.end() && !deleted; ++it)
		if (*it == s) {
			children.erase(it);
			deleted = true;
		}
}

void CompositeShape::draw() const {
	cout << "****************************\n\n";

	for (Shape* child : children)
		child->draw();

	cout << "\n\n****************************\n";
}
