#include "shape.h"
#include <exception>

using std::exception;

Shape::Shape(Shape* s) : parent(s) {}

void Shape::addChild(Shape *) { throw new exception(); }

void Shape::removeChild(Shape *) { throw new exception(); }
