#include "square.h"
#include "compositeshape.h"

int main()
{
	Square* s1 = new Square(5);
	CompositeShape* composite = new CompositeShape();
	composite->addChild(new Square(3,composite));
	composite->addChild(new Square(2,composite));

	CompositeShape* innerComposite = new CompositeShape(composite);
	composite->addChild(innerComposite);

	innerComposite->addChild(new Square(6,innerComposite));

	s1->draw();
	composite->draw();

	return 0;
}
