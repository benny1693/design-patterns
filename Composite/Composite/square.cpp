#include "square.h"
#include <iostream>

using std::cout;

void Square::drawHorizontal() const {
	for (int i = 0; i < side; ++i)
		cout << "+";
	cout << "\n";
}

Square::Square(int s, Shape *p) : Shape(p), side(s) {}

void Square::draw() const {

	drawHorizontal();

	for (int i = 0; i < side - 2; ++i) {
		cout << "+";
		for (int j = 0; j < side - 2; ++j)
			cout << " ";
		cout << "+\n";
	}

	drawHorizontal();
}
