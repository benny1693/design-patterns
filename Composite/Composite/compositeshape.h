#ifndef COMPOSITESHAPE_H
#define COMPOSITESHAPE_H

#include "shape.h"
#include <iostream>
#include <vector>

using std::vector;
using std::cout;

class CompositeShape : public Shape {
private:
	vector<Shape*> children;
public:
	CompositeShape(Shape* =nullptr);
	~CompositeShape();

	virtual void addChild(Shape*);
	virtual void removeChild(Shape*);

	virtual void draw() const;
};

#endif // COMPOSITESHAPE_H
