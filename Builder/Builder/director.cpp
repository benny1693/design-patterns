#include "director.h"

Director::Director() {}

ChristmasTree
Director::constructChristmasTreeWithoutStarAndComets(int balls,
																										 int letters) const {
	ChristmasTreeBuilder builder;
	return builder.buildBalls(balls).buildLetters(letters).getChristmasTree();
}

ChristmasTree
Director::constructChristmasTreeWithoutLettersAndBalls(bool star,
																											 int comets) const {
	ChristmasTreeBuilder builder;
	return builder.buildStar(star).buildComets(comets).getChristmasTree();
}

ChristmasTree Director::constructFullChristmasTree(int balls, bool star,
																									 int comets,
																									 int letters) const {
	ChristmasTreeBuilder builder;
	return builder.buildBalls(balls)
			.buildStar(star)
			.buildComets(comets)
			.buildLetters(letters)
			.getChristmasTree();
}

void Director::addTree(ChristmasTree tree) { trees.push_back(tree); }

void Director::printTrees() const {
	for (ChristmasTree tree : trees) {
		cout << tree << std::endl;
	}
}
