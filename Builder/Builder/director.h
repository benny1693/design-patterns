#ifndef DIRECTOR_H
#define DIRECTOR_H

#include "christmastreebuilder.h"

#include <vector>
#include <iostream>
using std::vector;
using std::cout;

class Director {
private:
	vector<ChristmasTree> trees;

public:
	Director();

	ChristmasTree constructChristmasTreeWithoutStarAndComets(int, int) const;
	ChristmasTree constructChristmasTreeWithoutLettersAndBalls(bool, int) const;
	ChristmasTree constructFullChristmasTree(int, bool, int, int) const;

	void addTree(ChristmasTree);

	void printTrees() const;
};

#endif // DIRECTOR_H
