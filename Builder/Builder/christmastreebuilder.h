#ifndef CHRISTMASTREEBUILDER_H
#define CHRISTMASTREEBUILDER_H

#include "christmastree.h"

class ChristmasTreeBuilder {
private:
	int balls;
	bool star;
	int comets;
	int letters;
public:
	ChristmasTreeBuilder();
	virtual ~ChristmasTreeBuilder() = default;

	virtual ChristmasTreeBuilder &buildBalls(int);
	virtual ChristmasTreeBuilder &buildStar(bool);
	virtual ChristmasTreeBuilder &buildComets(int);
	virtual ChristmasTreeBuilder &buildLetters(int);

	virtual ChristmasTree getChristmasTree() const;
};

#endif // CHRISTMASTREEBUILDER_H
