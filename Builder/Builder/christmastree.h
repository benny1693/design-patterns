#ifndef CHRISTMASTREE_H
#define CHRISTMASTREE_H

#include<iostream>
using std::ostream;
using std::endl;

class ChristmasTree {
	friend ostream& operator<<(ostream&,ChristmasTree);
private:
	int balls;
	bool star;
	int comets;
	int letters;

public:
	ChristmasTree();

	void setBalls(int value);
	void setStar(bool value);
	void setComets(int value);
	void setLetters(int value);
};

#endif // CHRISTMASTREE_H
