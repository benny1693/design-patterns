#include "christmastree.h"

void ChristmasTree::setBalls(int value) { balls = value; }

void ChristmasTree::setStar(bool value) { star = value; }

void ChristmasTree::setComets(int value) { comets = value; }

void ChristmasTree::setLetters(int value) { letters = value; }

ChristmasTree::ChristmasTree() {}

ostream &operator<<(ostream &os, ChristmasTree christmasTree) {
	return os << "Balls: " << christmasTree.balls << endl
						<< "Star: " << (christmasTree.star ? "yes" : "no") << endl
						<< "Comets: " << christmasTree.comets << endl
						<< "Letters: " << christmasTree.letters << endl;
}
