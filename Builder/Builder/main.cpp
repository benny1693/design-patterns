#include "director.h"

int main()
{

	Director director;

	director.addTree(director.constructChristmasTreeWithoutLettersAndBalls(true,5));
	director.addTree(director.constructChristmasTreeWithoutStarAndComets(10,9));
	director.addTree(director.constructFullChristmasTree(10,true,10,10));

	director.printTrees();

	cout << "end" << endl;

	return 0;
}
