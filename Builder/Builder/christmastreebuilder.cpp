#include "christmastreebuilder.h"

ChristmasTreeBuilder::ChristmasTreeBuilder()
		: balls(0), star(false), comets(0), letters(0) {}

ChristmasTreeBuilder &ChristmasTreeBuilder::buildBalls(int n) {
	balls = n;
	return *this;
}

ChristmasTreeBuilder &ChristmasTreeBuilder::buildStar(bool s) {
	star = s;
	return *this;
}

ChristmasTreeBuilder &ChristmasTreeBuilder::buildComets(int n) {
	comets = n;
	return *this;
}

ChristmasTreeBuilder &ChristmasTreeBuilder::buildLetters(int n) {
	letters = n;
	return *this;
}

ChristmasTree ChristmasTreeBuilder::getChristmasTree() const {
	ChristmasTree result;
	result.setBalls(balls);
	result.setComets(comets);
	result.setLetters(letters);
	result.setStar(star);

	return result;
}
