#ifndef CONTEXT_H
#define CONTEXT_H

#include<map>
using std::map;

class Context {
private:
	map<char,bool> context;
public:
	Context();
	void assignVariable(char,bool);
	bool getValue(char);
};

#endif // CONTEXT_H
