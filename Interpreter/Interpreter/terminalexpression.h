#ifndef TERMINALEXPRESSION_H
#define TERMINALEXPRESSION_H

#include "expression.h"

class TerminalExpression : public Expression {
private:
	char variable;
public:
	TerminalExpression(char);
	bool interpret(Context*) const;
};

#endif // TERMINALEXPRESSION_H
