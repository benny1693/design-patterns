#include "terminalexpression.h"

TerminalExpression::TerminalExpression(char var) : variable(var) {}

bool TerminalExpression::interpret(Context * context) const {
	return context->getValue(variable);
}
