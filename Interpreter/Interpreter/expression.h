#ifndef EXPRESSION_H
#define EXPRESSION_H

#include "context.h"

class Expression
{
public:
	Expression();
	virtual ~Expression() = default;

	virtual bool interpret(Context*) const =0;
};

#endif // EXPRESSION_H
