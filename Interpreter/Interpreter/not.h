#ifndef NOT_H
#define NOT_H

#include "expression.h"

class Not : public Expression {
private:
	Expression *expression;

public:
	Not(Expression*);

	bool interpret(Context *) const;
};

#endif // NOT_H
