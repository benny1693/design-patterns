#include "and.h"
#include "context.h"
#include "infer.h"
#include "not.h"
#include "or.h"
#include "terminalexpression.h"

#include <iostream>

int main() {
	Context *context = new Context();

	context->assignVariable('a', true);
	context->assignVariable('b', false);

	TerminalExpression *A = new TerminalExpression('a');
	TerminalExpression *B = new TerminalExpression('b');

	Expression *expression = new And(new Or(A, B), new Or(A, new Not(A)));

	std::cout << "Proposition: ((A or B) AND (A or !A))" << std::endl
						<< std::endl;

	bool val1 = context->getValue('a'), val2 = context->getValue('b');
	std::cout << "A value: " << val1 << " | "
						<< "B value: " << val2 << std::endl;
	std::cout << "Expected: " << ((val1 || val2) && (val1 || !val1)) << " | ";
	std::cout << "Obtained: " << expression->interpret(context) << std::endl
						<< std::endl;

	context->assignVariable('a', false);
	val1 = context->getValue('a');
	val2 = context->getValue('b');
	std::cout << "A value: " << val1 << " | "
						<< "B value: " << val2 << std::endl;
	std::cout << "Expected: " << ((val1 || val2) && (val1 || !val1)) << " | ";
	std::cout << "Obtained: " << expression->interpret(context) << std::endl
						<< "--------------------------------------" << std::endl;

	std::cout << "Proposition: ((A or B) -> (A and !A))" << std::endl
						<< std::endl;

	expression = new Infer(new Or(A, B), new And(A, new Not(A)));

	val1 = context->getValue('a');
	val2 = context->getValue('b');
	std::cout << "A value: " << val1 << " | "
						<< "B value: " << val2 << std::endl;
	std::cout << "Expected: " << ((val1 || val2) <= (val1 && !val1)) << " | ";
	std::cout << "Obtained: " << expression->interpret(context) << std::endl
						<< std::endl;

	context->assignVariable('a', true);
	val1 = context->getValue('a');
	val2 = context->getValue('b');
	std::cout << "A value: " << val1 << " | "
						<< "B value: " << val2 << std::endl;
	std::cout << "Expected: " << ((val1 || val2) <= (val1 && !val1)) << " | ";
	std::cout << "Obtained: " << expression->interpret(context) << std::endl;

	return 0;
}
