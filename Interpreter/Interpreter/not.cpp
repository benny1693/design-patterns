#include "not.h"

Not::Not(Expression* ex) : expression(ex) {}

bool Not::interpret(Context * context) const {
	return !(expression->interpret(context));
}
