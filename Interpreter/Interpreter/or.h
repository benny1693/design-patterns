#ifndef OR_H
#define OR_H

#include "expression.h"

class Or : public Expression {
private:
	Expression* expression1, *expression2;

public:
	Or(Expression*, Expression*);

	bool interpret(Context *context) const;
};

#endif // OR_H
