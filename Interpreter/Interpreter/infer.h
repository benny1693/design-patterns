#ifndef INFER_H
#define INFER_H

#include "expression.h"

class Infer : public Expression{
private:
	Expression* expression1, *expression2;

public:
	Infer(Expression*, Expression*);

	bool interpret(Context*) const;
};

#endif // INFER_H
