#include "infer.h"

Infer::Infer(Expression*ex1, Expression*ex2)
		: expression1(ex1), expression2(ex2) {}

bool Infer::interpret(Context *context) const {
	return expression1->interpret(context) <= expression2->interpret(context);
}
