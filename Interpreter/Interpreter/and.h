#ifndef AND_H
#define AND_H

#include "expression.h"

class And : public Expression {
private:
	Expression* expression1, *expression2;

public:
	And(Expression*, Expression*);

	bool interpret(Context*) const;
};

#endif // AND_H
