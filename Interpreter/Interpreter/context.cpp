#include "context.h"

Context::Context() {}

void Context::assignVariable(char variable, bool value) {
	if (context.find(variable) == context.end())
		context.insert(std::pair<char, bool>(variable, value));
	else
		context.at(variable) = value;
}

bool Context::getValue(char variable) { return context.at(variable); }
