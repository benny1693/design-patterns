#ifndef STATE_H
#define STATE_H

#include<string>

class Switch;

class State {
public:
	State();
	virtual ~State() = default;
	void switchState(Switch *, State *) const;
	virtual std::string printState() const = 0;
};

#endif // STATE_H
