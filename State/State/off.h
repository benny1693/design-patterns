#ifndef OFF_H
#define OFF_H

#include "state.h"

class Off : public State {
public:
	Off();
	std::string printState() const;
};

#endif // OFF_H
