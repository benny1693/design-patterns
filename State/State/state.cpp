#include "state.h"
#include "switch.h"

State::State() {}

void State::switchState(Switch *sw, State *st) const { sw->changeState(st); }
