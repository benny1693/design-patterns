#ifndef ON_H
#define ON_H

#include "state.h"

class On : public State {
public:
	On();
	std::string printState() const;
};

#endif // ON_H
