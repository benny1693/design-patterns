#ifndef SWITCH_H
#define SWITCH_H

#include "state.h"
#include <string>
using std::string;

class Switch {
private:
	string name;
	State *state;

public:
	Switch(string, State *);
	void changeState(State *);
	State* getState() const;

	void printSwitch() const;
};

#endif // SWITCH_H
