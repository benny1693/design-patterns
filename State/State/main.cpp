#include "off.h"
#include "on.h"
#include "switch.h"
#include <iostream>

int main() {
	On *on = new On();
	Off *off = new Off();
	Switch *sw = new Switch("1", off);

	char c;
	std::cin >> c;
	while (c != 'q') {
		if (sw->getState() == on)
			on->switchState(sw, off);
		else
			off->switchState(sw, on);

		sw->printSwitch();
		std::cin >> c;
	}

	return 0;
}
