#include "switch.h"
#include <iostream>

Switch::Switch(std::string n, State *s) : name(n), state(s) {}

void Switch::changeState(State *s) { state = s; }

State *Switch::getState() const { return state; }

void Switch::printSwitch() const {
	std::cout << "Switch " + name + ": " + state->printState() << std::endl;
}
