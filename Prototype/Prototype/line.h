#ifndef LINE_H
#define LINE_H

#include "graphicelement.h"

class Line : public GraphicElement {
public:
	Line(u_int = 0U);

	void printElement() const;

	Line* clone() const;
};

#endif // LINE_H
