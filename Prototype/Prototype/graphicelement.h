#ifndef GRAPHICELEMENT_H
#define GRAPHICELEMENT_H

#include<iostream>

class GraphicElement {
private:
	u_int dimension;
public:
	GraphicElement(u_int = 0U);
	virtual ~GraphicElement() = default;

	u_int getDimension() const;

	virtual void printElement() const = 0;

	virtual GraphicElement* clone() const = 0;
};

#endif // GRAPHICELEMENT_H
