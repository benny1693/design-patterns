#include "graphictool.h"
#include "line.h"
#include "square.h"

int main() {
	GraphicTool tool;

	tool.addElement(1,new Square(7));
	tool.addElement(2,new Square(3));
	tool.addElement(3,new Line(5));

	tool.draw(1);
	tool.draw(3);
	tool.draw(2);
	tool.draw(1);

	tool.addElement(4,tool.removeElement(2));

	tool.draw(4);

	return 0;
}
