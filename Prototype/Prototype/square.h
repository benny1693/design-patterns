#ifndef SQUARE_H
#define SQUARE_H

#include "graphicelement.h"

class Square : public GraphicElement {
private:
	void printHorizontal() const;
	void printSides() const;
public:
	Square(u_int = 0U);

	void printElement() const;

	Square *clone() const;
};

#endif // SQUARE_H
