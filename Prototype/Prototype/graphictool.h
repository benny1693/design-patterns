#ifndef GRAPHICTOOL_H
#define GRAPHICTOOL_H

#include<map>
#include "graphicelement.h"

using std::map;

class GraphicTool {
private:
	map<int,GraphicElement*> elementRegister;
public:
	GraphicTool();

	void addElement(int,GraphicElement*);
	GraphicElement* removeElement(int);
	GraphicElement* getElement(int);

	void draw(int) const;
};

#endif // GRAPHICTOOL_H
