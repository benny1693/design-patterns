#include "graphictool.h"

GraphicTool::GraphicTool() {}

void GraphicTool::addElement(int id, GraphicElement *element) {
	elementRegister.insert(std::pair<int, GraphicElement *>(id, element));
}

GraphicElement *GraphicTool::removeElement(int id) {
	GraphicElement *removed = elementRegister.at(id)->clone();
	elementRegister.erase(id);
	return removed;
}

GraphicElement *GraphicTool::getElement(int id) {
	return elementRegister.at(id);
}

void GraphicTool::draw(int id) const {
	elementRegister.at(id)->printElement();
}
