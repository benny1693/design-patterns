#include "line.h"

Line::Line(u_int d) : GraphicElement(d) {}

void Line::printElement() const {
	for (u_int i = 0U; i < getDimension(); ++i)
		std::cout << "+";
	std::cout << std::endl;
}

Line *Line::clone() const { return new Line(*this); }
