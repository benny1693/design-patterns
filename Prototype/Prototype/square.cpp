#include "square.h"

void Square::printHorizontal() const {
	for (u_int i = 0U; i < getDimension(); ++i)
		std::cout << "+";
	std::cout << std::endl;
}

void Square::printSides() const
{
	for (u_int i = 0U; i < getDimension() - 2; ++i){
		std::cout << "+";

		for (u_int j = 0U; j < getDimension() - 2; ++j)
			std::cout << " ";

		std::cout << "+" << std::endl;;
	}
}

Square::Square(u_int d) : GraphicElement(d) {}

void Square::printElement() const {
	printHorizontal();
	printSides();
	printHorizontal();
}

Square *Square::clone() const { return new Square(*this); }
