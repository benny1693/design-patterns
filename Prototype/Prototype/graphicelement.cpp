#include "graphicelement.h"

GraphicElement::GraphicElement(u_int d) : dimension(d) {}

u_int GraphicElement::getDimension() const { return dimension; }
